const http = require('http');
const fs = require('fs');
const url = require('url');

const dir = './file';
  
  if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir);
  }

module.exports = () => {
  
  http.createServer(function(request, response) {
    const { pathname, query } = url.parse(request.url, true);
  
    if (request.method === 'POST') {
      
      const message = `New file with name call ${query.filename} saved`;
  
      fs.writeFile(`./${pathname}/${query.filename}`, query.content, 'utf8', err => {

        if (err) {
          response.writeHead(400, {'Content-type': 'text/html'});
          response.end(`File ${pathname} not exist`);
          return;
        };

        if (!query.filename || !query.content) {
          response.writeHead(400, {'Content-type': 'text/html'});
          response.end(`Wrong params`);
          return;
        };

        updateLogs(message);
        response.writeHead(200, {'Content-type': 'text/html'});
        response.end(`File ${query.filename} was added`);
        return;
      });

    } else {

      if (pathname === '/logs') {
  
        fs.readFile(`./${pathname}.json`, (err, content) => {

          if (err) {
            response.writeHead(400, {'Content-type': 'text/html'});
            response.end(`No file with name call ${pathname}.json found`);
            return;
          }

          response.writeHead(200, {'Content-type': 'application/json'});
          response.end((query.from || query.to) ? getLogsRange(content, query.from, query.to) : content);
          return;
        });
      } else {
        const message = `File with name ${pathname} read`;

        fs.readFile(`./${pathname}`, (err, content) => {
    
          if (err) {
            response.writeHead(400, {'Content-type': 'text/html'});
            response.end(`No file with name call ${pathname} found`);
            return;
          }

          updateLogs(message);
          response.writeHead(200, {'Content-type': 'text/html'});
          response.end(content);
          return;
        });
      }
    }
  
  }).listen(process.env.PORT || 8080);
  
  
  function updateLogs(message) {

    fs.readFile('logs.json', (err, data) => {
      
      if (err) {
        const logs = {
          logs: [
            {
              message,
              time: Date.now()
            }
          ]
        };
        fs.writeFileSync('logs.json', JSON.stringify(logs), 'utf8');
      } else {
        const logs = JSON.parse(data.toString());
        logs.logs.push({
          message,
          time: Date.now()
        });
        fs.writeFileSync('logs.json', JSON.stringify(logs), 'utf8');
      }
    });
  }
  
  function getLogsRange(content, fromTimestamps, toTimestamps) {
    const logs = JSON.parse(content.toString());
    const result = logs.logs.filter(log => {

      if (!fromTimestamps && toTimestamps) {
        return log.time <= +toTimestamps;
      } else if (fromTimestamps && !toTimestamps) {
        return log.time >= +fromTimestamps;
      } else {
        return log.time >= +fromTimestamps && log.time <= +toTimestamps;
      }
    });

    logs.logs = result;
    return JSON.stringify(logs);
  }
}